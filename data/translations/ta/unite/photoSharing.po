# Strings used in Opera Unite applications.
# Copyright (C) 2009 Opera Software ASA
# This file is distributed under the same license as Opera Unite applications.
# Anders Sjögren <anderss@opera.com>, Kenneth Maage <kennethm@opera.com>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-08-31 11:13-02:00\n"
"PO-Revision-Date: 2009-11-06 00:18+0530\n"
"Last-Translator: Chitra Muralidharan <muralidharan.chitra@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"MIME-Version: 1.0\n"

#. Notification message shown on the computer running the PhotoSharing when new thumbnails and preview images are generated, which might be intensive.
#: photosharing.js
msgid "Generating thumbnails. Your computer may slow down."
msgstr "சிறுஉருவங்களை உருவாக்குகிறது. உங்கள் கணினியின் வேகம் குறையக்கூடும்."

#. Error heading when a link is misspelled or the photo or album doesn't exist.
#: templates/resourceNotFound.html
msgid "Folder or photo not found."
msgstr "கோப்புறை அல்லது புகைப்படம் காணவில்லை"

#. Link that navigates to the picture before this one.
#: templates/index.html
msgid "Previous"
msgstr "முந்தையது"

#. Gray text shown in place of "Previous" link, when the picture is the first one in an album
#: templates/index.html
msgid "First"
msgstr "முதலாவது"

#. Link that navigates to the picture after this one.
#: templates/index.html
msgid "Next"
msgstr "அடுத்தது"

#. Gray text shown in place of "Next" link, when the picture is the last one in an album
#: templates/index.html
msgid "Last"
msgstr "கடைசி"

#. Link that lets a visitor save the original file
#: templates/index.html
msgid "Download"
msgstr "பதிவிறக்கம்"

#. Control that shows the picture in original size
#: templates/index.html
msgid "Original size"
msgstr "அசல் அளவு"

#. A link that displays the photos in a slideshow.
#: templates/index.html
msgid "Slideshow"
msgstr "ஸ்லைடு காட்சி"

#. A text that describes how to stop a slideshow.
#: templates/index.html
msgid "Click anywhere to stop the slideshow."
msgstr "ஸ்லைடு காட்சியை நிறுத்துவதற்கு எங்காவது கிளிக் செய்யவும்."

#. From the line "Page [1] 2 3" below the view of thumbnails
#: templates/index.html
msgid "Page"
msgstr "பக்கம்"

#. Singular case
#. From the line "12 images and 3 folders" below the view of thumbnails
#: templates/index.html
msgid "1 image"
msgstr "1 உருவம்"

#. Plural case
#. From the line "12 images and 3 folders" below the view of thumbnails
#: templates/index.html
msgid "{counter} images"
msgstr "{counter} உருவங்கள்"

#. From the line "12 images and 3 folders" below the view of thumbnails
#: templates/index.html
msgid "and"
msgstr "மற்றும்"

#. Singular case
#. From the line "12 images and 3 folders" below the view of thumbnails
#: templates/index.html
msgid "1 folder"
msgstr "1 கோப்புறை"

#. Plural case
#. From the line "12 images and 3 folders" below the view of thumbnails
#: templates/index.html
msgid "{counter} folders"
msgstr "{counter} கோப்புறைகள்"

#. From the line "Image 2 of 18" below the page view of a photo
#: templates/index.html
msgid "Image {imageIndex} of {imagesCount}"
msgstr "{imagesCount}-இல் {imageIndex} உருவம்"

# grammar
#. Message shown when the original share folder selected by the owner can't be accessed
#. Properties... text comes from the right-click menu of the application in the Unite panel.
#: templates/noSharedMountpoint.html
msgid "Folder not found. To select a new one, right-click <STRONG>{serviceName}</STRONG> in the Unite panel, and choose <STRONG>Properties</STRONG>"
msgstr "கோப்புறை காணவில்லை. புதிய ஒன்றைத் தேர்ந்தெடுக்க, Unite Panel-இல் உள்ள <STRONG>{சேவைப்பெயர்}</STRONG> வலது கிளிக் செய்து, <STRONG>தன்மைகள்</STRONG> தெரிவுசெய்யவும்கோப்புறை காணவில்லை. புதிய ஒன்றைத் தேர்ந்தெடுக்க, Unite Panel-இல் உள்ள <STRONG>{சேவைப்பெயர்}</STRONG> மீது வலது கிளிக் செய்து, <STRONG>தன்மைகள்</STRONG>-ஐத் தெரிவுசெய்யவும்."

